ARG BASEIMAGE=python:alpine3.14
#To use another base image, use "--build-arg BASEIMAGE=localimage:latest" in the build command

ARG DMAGICIMAGE=registry.gitlab.com/fumail/domainmagic:latest
# domainmagic image to use

#==                             ==#
#= Prepare, install dependencies =#
#==                             ==#
FROM $BASEIMAGE as fuglu-prepare-base

ENV PYTHONPATH=/fuglu
WORKDIR /fuglu

RUN apk add libffi libxml2 libxslt libmagic openssl mariadb-connector-c python3 py3-pip make netcat-openbsd unrar
RUN pip install --upgrade pip setuptools wheel


#--
# install dev packages, build and install python modules
#--
FROM fuglu-prepare-base as fuglu-prepare-build

RUN apk add automake autoconf libtool gcc musl-dev libffi-dev g++ libxml2-dev libxslt-dev openssl-dev mariadb-dev mysql-client python3-dev
RUN BUILD_LIB=1 pip install ssdeep


#--
# ssdeep installation
# apk add automake autoconf libtool gcc make musl-dev libffi libffi-dev g++
# BUILD_LIB=1 pip install ssdeep
#--

#--
# lxml installation
# apk add libxml2 libxml2-dev libxslt libxslt-dev
# pip install lxml


#---
# we should check if we can use a multistage build to copy the compiled libraries to the
# new image, so we don't have all dev-tools in the image
# image size: 427MB
# image size CentOS based Dockerfile: 1.16GB
#---

RUN pip install rarfile "sqlalchemy<2.0.0" python-magic!=0.4.23 pyspf py3dns mock ssdeep redis geoip2 beautifulsoup4 lxml pysrs pylzma mysqlclient dkimpy authres dmarc aioprocessing pyyaml pytest pysocks

# - Install python3 branch of libmilter
RUN pip install distro && \
            pip install https://github.com/crustymonkey/python-libmilter/archive/python3.zip

# make package with all dependencies to install in clean container
RUN tar czf /tmp/site-packages.tgz `python -c 'import site; print(site.getsitepackages()[0])'` /usr/local/bin/pytest



# Hack because arg can not be used in COPY --from=${DMAGICIMAGE} ...
FROM ${DMAGICIMAGE} as domainmagicimage

#==           ==#
#= Final image =#
#==           ==#
#
#---
# now put final image together with all necessary runtime libs and modules
#---
FROM fuglu-prepare-base as fuglu-source

# copy all python packages
COPY --from=fuglu-prepare-build /tmp/site-packages.tgz /tmp/site-packages.tgz
RUN tar xzf /tmp/site-packages.tgz -C /

#--
# add domainmagic
#--
RUN mkdir /domainmagic
COPY --from=domainmagicimage /src/domainmagic /domainmagic/domainmagic
ENV PYTHONPATH="${PYTHONPATH}:/domainmagic"
# initialise /tmp/tlds-alpha-by-domain.txt
RUN python -c "from domainmagic import tld; _ = tld.TLDMagic()"

#--
#- Add fuglu source -#
#--

# fuglu
COPY ./fuglu/src/. /fuglu

# with config
from fuglu-source as fuglu-configured

VOLUME /etc/fuglu

#10025: standard incoming mail
#10028: standard milter port
#10888: debug port
EXPOSE 10025 10888 10028

COPY docker/fuglu/conf/. /etc/fuglu/

# currently cannot run as non-root, else gitlab-ci.yml fails in stage pages
#USER nobody
CMD python /fuglu/startscript/fuglu --foreground --lint && python3 /fuglu/startscript/fuglu --foreground

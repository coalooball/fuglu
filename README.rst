FuGlu is a modular milter/pre-queue/after-queue content filter written in Python that
acts as the glue application between the MTA and Spam-, Virus-, and other scanners.
It can be used to filter spam, viruses, unwanted attachments, do other content filtering,
and various mail manipulation operations.

FuGlu supports SPF, DKIM, DMARC, and ARC checks an can generate DKIM and ARC signature headers.

FuGlu focuses on being solid, easy to manage, extend, debug and monitor.

Quick links:

 * `Overview and documentation <https://fumail.gitlab.io/fuglu/>`_
 * `Download <https://pypi.python.org/pypi/fuglu/>`_
 * `Issue Tracker <https://gitlab.com/fumail/fuglu/issues>`_
 * `Extra Plugins Repository <https://gitlab.com/fumail/fuglu-extra-plugins/>`_

.. image:: https://badge.fury.io/py/fuglu.svg
    :target: https://pypi.python.org/pypi/fuglu/
    :alt: Latest PyPI version

.. image:: https://gitlab.com/fumail/fuglu/badges/master/pipeline.svg
    :target: https://gitlab.com/fumail/fuglu/pipelines
    :alt: Build status

# -*- coding: utf-8 -*-
from .unittestsetup import TESTDATADIR
import unittest
import logging
import sys
import os
from fuglu.shared import Suspect, FuConfigParser

from fuglu.plugins.mailcopy import IMAPCopyPlugin
from .dummyimap import DummyIMAP
from unittest.mock import patch



def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


class IMAPCopyTest(unittest.TestCase):
    def setUp(self):
        self.section = "IMAPCopyPlugin"

        regexcontent = r"to_domain       unittests\.fuglu\.org     " \
                       r"imap://spam@example.org:secretpass@mymailserver.example.org/INBOX.archive"

        regexfile = '/tmp/imapcopy.regex'
        with open(regexfile, 'w') as f:
            f.write(regexcontent)

        self.config=FuConfigParser()
        self.config.add_section(self.section)
        self.config.set(self.section, 'imapcopyrules', regexfile)
        self.config.set(self.section, 'storeoriginal', '0')


    def tearDown(self):
        pass

    @patch('imaplib.IMAP4')
    def test_basecopy(self, mock_imap_constructor):
        """Test is message is copied"""

        setup_module()

        # answers for dummy IMAP
        imap_answers = {"IMAP4::['mymailserver.example.org', 143]": [[]],
                        "login::['username', 'password']": [['OK', ['Logged in']]],
                        "select::['INBOX.archive']": [['OK', ['790']]],
                        "append::['INBOX.archive', None, None, '']": [['OK', ['7xx']]],
                        'logout::[]': [[]]
                        }

        dummy_imap = DummyIMAP(imap_answers)
        # enable debug output
        dummy_imap.debug = True
        # ignore the content in the "append" command
        dummy_imap.append_cmd_ignorecontent = True
        # store mail in append command
        dummy_imap.append_store = True

        mock_imap_constructor.side_effect = dummy_imap.constructor_imap4

        candidate = IMAPCopyPlugin(self.config, self.section)

        myclass = self.__class__.__name__
        function_name_as_string = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass, function_name_as_string)
        logger = logging.getLogger(loggername)

        logger.debug("Read file content")
        filecontent = open(os.path.join(TESTDATADIR, 'mail_base64.eml'), 'rb').read()

        logger.debug("Create suspect")
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'mail_base64.eml'))
        suspect.set_source(filecontent)

        logger.debug("Examine")
        self.config.set(self.section, 'storeoriginal', '0')
        candidate.run_examine(suspect)
        self.assertGreater(len(dummy_imap.append_messages), 0)
        self.assertEqual(filecontent, dummy_imap.append_messages[0])
        self.assertTrue(dummy_imap.is_empty())

    @patch('imaplib.IMAP4')
    def test_with_badsubject(self, mock_imap_constructor):
        """Test is message is copied"""

        setup_module()

        # answers for dummy IMAP
        imap_answers = {"IMAP4::['mymailserver.example.org', 143]": [[]],
                        "login::['username', 'password']": [['OK', ['Logged in']]],
                        "select::['INBOX.archive']": [['OK', ['790']]],
                        "append::['INBOX.archive', None, None, '']": [['OK', ['7xx']]],
                        'logout::[]': [[]]
                        }

        dummy_imap = DummyIMAP(imap_answers)
        # enable debug output
        dummy_imap.debug = True
        # ignore the content in the "append" command
        dummy_imap.append_cmd_ignorecontent = True
        # store mail in append command

        mock_imap_constructor.side_effect = dummy_imap.constructor_imap4

        candidate = IMAPCopyPlugin(self.config, self.section)

        myclass = self.__class__.__name__
        function_name_as_string = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass, function_name_as_string)
        logger = logging.getLogger(loggername)

        logger.debug("Read file content")
        filecontent = open(os.path.join(TESTDATADIR, 'mail_badsubject.eml'), 'r').read().encode("utf-8", "strict")

        logger.debug("Create suspect")
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_source(filecontent)

        logger.debug("Examine")
        self.config.set(self.section, 'storeoriginal', '0')
        candidate.run_examine(suspect)
        self.assertTrue(dummy_imap.is_empty())

# -*- coding: UTF-8 -*-
from .unittestsetup import TESTDATADIR
import unittest
import tempfile
import os
from fuglu.plugins.archive import ArchivePlugin
from fuglu.shared import FuConfigParser


class ArchiveTestcase(unittest.TestCase):

    def setUp(self):
        self.tempfiles = []

        config = FuConfigParser()
        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '1')

        config.add_section('ArchivePlugin')
        config.set('ArchivePlugin', 'archivebackends', 'localdir')
        config.set('ArchivePlugin', 'local_archivedir', '/tmp')
        config.set('ArchivePlugin', 'local_subdirtemplate', '')
        config.set('ArchivePlugin', 'local_filenametemplate', '${id}.eml')
        config.set('ArchivePlugin', 'local_storeoriginal', '1')
        config.set('ArchivePlugin', 'local_chmod', '700')
        config.set('ArchivePlugin', 'local_chown', '')
        config.set('ArchivePlugin', 'local_chgrp', '')

        fp, tempfilename = tempfile.mkstemp(suffix='archive', prefix='fuglu-unittest', dir='/tmp')
        fp = open(tempfilename, 'w')
        fp.write('From unittests.fuglu.org')
        self.tempfiles.append(tempfilename)
        config.set('ArchivePlugin', 'archiverules', tempfilename)

        self.config = config
    
    
    def tearDown(self):
        for tf in self.tempfiles:
            os.remove(tf)
    
    
    def test_original_message(self):
        """Test if the original message gets archived correctly"""
        from fuglu.shared import Suspect
        import shutil
        import tempfile

        fp, tempfilename = tempfile.mkstemp(suffix='archive', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy(TESTDATADIR + '/helloworld.eml', tempfilename)
        self.tempfiles.append(tempfilename)

        self.config.set('ArchivePlugin', 'storeoriginal', '1')
        candidate = ArchivePlugin(self.config)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tempfilename)
        origmessage = suspect.get_source()

        # modify the mesg
        msgrep = suspect.get_message_rep()
        msgrep['X-Changed-Something'] = 'Yes'
        suspect.setMessageRep(msgrep)

        result, errors = candidate.do_archive(suspect)
        filename = result.get('localdir', {}).get('filepath')
        self.assertTrue(filename is not None and filename)
        self.assertDictEqual(errors, {})

        self.tempfiles.append(filename)
        archivedmessage = open(filename, 'rb').read()

        self.assertEqual(origmessage.strip(), archivedmessage.strip()), 'Archived message has been altered'
    
    
    def test_modified_message(self):
        """Test if the modified message gets archived correctly"""
        from fuglu.shared import Suspect
        import shutil
        import tempfile

        fp, tempfilename = tempfile.mkstemp(suffix='archive', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy(TESTDATADIR + '/helloworld.eml', tempfilename)
        self.tempfiles.append(tempfilename)

        self.config.set('ArchivePlugin', 'local_storeoriginal', '0')
        candidate = ArchivePlugin(self.config)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tempfilename)
        origmessage = suspect.get_source()
        # modify the mesg
        msgrep = suspect.get_message_rep()
        msgrep['X-Changed-Something'] = 'Yes'
        suspect.setMessageRep(msgrep)

        result, errors = candidate.do_archive(suspect)
        filename = result.get('localdir', {}).get('filepath')
        self.assertTrue(filename is not None and filename)
        self.assertDictEqual(errors, {})

        self.tempfiles.append(filename)
        archivedmessage = open(filename, 'r').read()
        self.assertNotEqual(origmessage.strip(), archivedmessage.strip()), 'Archived message should have stored modified message'

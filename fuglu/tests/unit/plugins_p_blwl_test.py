# -*- coding: UTF-8 -*-
import os
import unittest
from .unittestsetup import TESTDATADIR
from fuglu.plugins.p_blwl import BlockWelcomeEntry, FugluBlockWelcome, AutoListAppender, STAGE_PREPENDER
from fuglu.shared import Suspect, FuConfigParser, DUNNO


class FugluBlockWelcomeTest(FugluBlockWelcome):
    def __init__(self, config, section, listings):
        super().__init__(config, section)
        self.listing_data = listings
    
    def _get_listings(self, suspectid=''):
        listings = {}
        for item in self.listing_data:
            listing = BlockWelcomeEntry(**item)
            scope = item['scope']
            try:
                listings[scope].append(listing)
            except KeyError:
                listings[scope] = [listing]
        return listings


class FugluBlockWelcomeTestCase(unittest.TestCase):
    
    def setUp(self):
        config = FuConfigParser()
        section = 'BlockWelcome'
        config.add_section(section)
        config.set(section, 'fublwl_eval_order', 'user,domain,global')
        config.set(section, 'fublwl_update_hitcount', 'False')
        config.set(section, 'fublwl_header_checks', 'False')
        config.set(section, 'fublwl_header_host_only', 'False')
        config.set(section, 'fublwl_debug', 'True')
        config.set(section, 'fublwl_restapi_endpoint', '')
        config.set(section, 'fublwl_restapi_key_map', '')
        config.set(section, 'fublwl_json_file', '/dev/zero')
        self.config = config
        self.section = section
    
    def tearDown(self):
        pass
    
    def test_listing_ip(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': 32, 'scope': '$GLOBAL', 'sender_addr': None, 'sender_host': '8.8.8.8'},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'ip {suspect.clientinfo[1]} not welcomelisted')
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '9.9.9.9', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), f'ip {suspect.clientinfo[1]} is welcomelisted')
        
        
    def test_listing_sender_email(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': -1, 'scope': '$GLOBAL', 'sender_addr': 'sender@unittests.fuglu.org', 'sender_host': None},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'sender {suspect.from_address} not welcomelisted')
        
        suspect = Suspect('somebody@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), f'sender {suspect.from_address} is welcomelisted')
    
    
    def test_listing_sender_dom(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': -1, 'scope': '$GLOBAL', 'sender_addr': '%unittests.fuglu.org', 'sender_host': None},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'sender {suspect.from_domain} not welcomelisted')
        
        suspect = Suspect('somebody@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), f'sender {suspect.from_domain} not welcomelisted')
        
        suspect = Suspect('sender@othertests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect, STAGE_PREPENDER)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), f'sender {suspect.from_domain} is welcomelisted')


class AutoListAppenderTest(AutoListAppender):
    def _store_redis(self, suspect, rediskey, increment):
        self.rediskey = rediskey
        self.increment = increment

class TestAutoListAppender(unittest.TestCase):
    def setUp(self):
        config = FuConfigParser()
        section = 'AutoListAppender'
        config.add_section(section)
        config.set(section, 'increment_overrides', os.path.join(TESTDATADIR, "autolist_overrides.txt"))
        self.config = config
        self.section = section
    
    def test_overrides(self):
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        candidate = AutoListAppenderTest(self.config, self.section)
        increment = candidate._get_increment(suspect, 'sender@unittests.fuglu.org', False)
        self.assertEqual(increment, 1)
        increment = candidate._get_increment(suspect, 'sender@unittests.fuglu.org', True)
        self.assertEqual(increment, 10)
        increment = candidate._get_increment(suspect, 'sender@example.fuglu.org', True)
        self.assertEqual(increment, 15)
        increment = candidate._get_increment(suspect, 'special@example.fuglu.org', True)
        self.assertEqual(increment, 20)
        
    
    def test_full(self):
        suspect = Suspect('sender@example.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        candidate = AutoListAppenderTest(self.config, self.section)
        candidate.process(suspect, DUNNO)
        self.assertEqual(candidate.increment, 1)
        self.assertEqual(candidate.rediskey, 'recipient@unittests.fuglu.org\nsender@example.fuglu.org')
    
    
# -*- coding: UTF-8 -*-
import unittest
import os
from fuglu.shared import Suspect, FuConfigParser
from fuglu.extensions.sql import get_session, DBConfig, SQL_EXTENSION_ENABLED, text
import tempfile

class DBConfigTestCase(unittest.TestCase):

    """Test Templates"""

    def setUp(self):
        self.testfile = "/tmp/fuglu_override_test.db"
        if os.path.exists(self.testfile):
            os.remove(self.testfile)
        # important: 4 slashes for absolute paths!
        self.testdb = f"sqlite:///{self.testfile}"

        config = FuConfigParser()
        config.add_section('databaseconfig')
        config.set('databaseconfig', 'globalscope', '$GLOBAL')
        config.set('databaseconfig', 'dbpriority', 'sql')
        config.set('databaseconfig', 'dbconnectstring', self.testdb)
        config.set('databaseconfig', "sql",
                   "SELECT value FROM fugluconfig WHERE section=:section AND option=:option AND scope IN ('$GLOBAL','%'||:to_domain,:to_address) ORDER BY SCOPE DESC")
        self.config = config
        self.create_database()

    def create_database(self):
        sql = """
        CREATE TABLE fugluconfig (
           scope varchar(255) NOT NULL,
           section varchar(255) NOT NULL,
           option varchar(255) NOT NULL,
           value varchar(255) NOT NULL
        )
        """
        self.exec_sql(sql)

    def clear_table(self):
        self.exec_sql("DELETE FROM fugluconfig")

    def exec_sql(self, sql, values=None):
        if not SQL_EXTENSION_ENABLED:
            return
        if values is None:
            values = {}
        session = get_session(self.testdb)
        session.execute(text(sql), values)
        session.remove()

    def insert_override(self, scope, section, option, value):
        sql = "INSERT INTO fugluconfig (scope,section,option,value) VALUES (:scope,:section,:option,:value)"
        values = dict(scope=scope, section=section, option=option, value=value)
        self.exec_sql(sql, values)

    def tearDown(self):
        if os.path.exists(self.testfile):
            os.remove(self.testfile)

    def test_user_override(self):
        """Test basic config overrdide functionality"""
        if not SQL_EXTENSION_ENABLED:
            return
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')

        self.clear_table()
        self.insert_override('recipient@unittests.fuglu.org', 'testsection', 'override', '200')
        self.insert_override('%unittests.fuglu.org', 'testsection', 'override', '300')
        self.insert_override('$GLOBAL', 'testsection', 'override', '400')
        self.assertEqual(int(candidate.get('testsection', 'nooverride')), 100)
        self.assertEqual(int(candidate.get('testsection', 'override')), 200)

    def test_domain_override(self):
        """Test basic config overrdide functionality"""
        if not SQL_EXTENSION_ENABLED:
            return
        suspect = Suspect('sender@unittests.fuglu.org', 'someotherrec@unittests.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')

        self.clear_table()
        self.insert_override('recipient@unittests.fuglu.org', 'testsection', 'override', '200')
        self.insert_override('%unittests.fuglu.org', 'testsection', 'override', '300')
        self.insert_override('$GLOBAL', 'testsection', 'override', '400')
        self.assertEqual(int(candidate.get('testsection', 'nooverride')), 100)
        self.assertEqual(int(candidate.get('testsection', 'override')), 300)

    def test_global_override(self):
        """Test basic config overrdide functionality"""
        if not SQL_EXTENSION_ENABLED:
            return
        suspect = Suspect('sender@unittests.fuglu.org', 'someotherrec@unittests2.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')

        self.clear_table()
        self.insert_override('recipient@unittests.fuglu.org', 'testsection', 'override', '200')
        self.insert_override('%unittests.fuglu.org', 'testsection', 'override', '300')
        self.insert_override('$GLOBAL', 'testsection', 'override', '400')
        self.assertEqual(int(candidate.get('testsection', 'nooverride')), 100)
        self.assertEqual(int(candidate.get('testsection', 'override')), 400)


class DBConfigYamlTestCase(unittest.TestCase):

    """Test Templates"""

    def setUp(self):
        yamldata = """
testsection:
  override:
    recipient@unittests.fuglu.org: 200
  boolvalue:
    recipient@unittests.fuglu.org: false
        """
        
        fp, self.testfile = tempfile.mkstemp(suffix='sqlyaml', prefix='fuglu-unittest', dir='/tmp')
        with open(self.testfile, 'w') as fp:
            fp.write(yamldata)

        config = FuConfigParser()
        config.add_section('databaseconfig')
        config.set('databaseconfig', 'globalscope', '$GLOBAL')
        config.set('databaseconfig', 'dbpriority', 'yaml')
        config.set('databaseconfig', 'yaml_filepath', self.testfile)
        self.config = config

    def test_user_override(self):
        """Test basic config overrdide functionality"""
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')

        candidate = DBConfig(self.config, suspect)

        candidate.add_section('testsection')
        candidate.set('testsection', 'nooverride', '100')
        candidate.set('testsection', 'override', '100')
        candidate.set('testsection', 'boolvalue', 'True')

        self.assertEqual(100, candidate.getint('testsection', 'nooverride'))
        self.assertEqual(200, candidate.getint('testsection', 'override'))
        self.assertFalse(candidate.getboolean('testsection', 'boolvalue'))

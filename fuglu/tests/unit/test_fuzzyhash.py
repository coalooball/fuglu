import sys
import logging

import unittest
from fuglu.plugins.antivirus import FuzzyHashCheck, HAVE_SSDEEP
from io import BytesIO
from fuglu.stringencode import force_uString
from fuglu.shared import FuConfigParser
from os.path import join

# test mails to import
from .unittestsetup import TESTDATADIR
from .storedmails import mail_fakeEXEinrar, mail_fakeEXEinzip
from unittest.mock import patch
from unittest.mock import MagicMock



def get_session_mock(*a, **kw):
    # mock the session object
    m = MagicMock()
    # and the execute function of the session object
    m.execute.return_value = [["3:hMCEu3Q3Fx:huuQ3Fx", "fakeEXE.exe"]]
    return m

def get_hashes_mock():
    return {"3:hMCEu3Q3Fx:huuQ3Fx": "fakeEXE.exe"}

def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


class FuzzyhashPlugintest(unittest.TestCase):
    """
    Test fuzzyhash plugin
    - SSDEEPcheck which will hash the files in attached archives
    - SSDEEPBody which will hash the biggest text part found in the mail body
    """

    @patch('fuglu.plugins.antivirus', side_effect=get_session_mock)
    def test_zipattachment(self,get_session_func):
        """Test if exe is found in zip - attachment and hashed correctly"""
        if not HAVE_SSDEEP:
            return

        myclass = self.__class__.__name__
        functionNameAsString = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass,functionNameAsString)
        logger = logging.getLogger(loggername)

        logger.debug("Config parser")
        confdefaults = r"""
[test]
dbconnectstring=
maxsize=1000000000000
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
threshold=75
virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf = FuConfigParser()
        conf.read_string(force_uString(confdefaults))

        logger = logging.getLogger(loggername)
        logger.debug("Read file content")
        filecontent = BytesIO(mail_fakeEXEinzip).read()

        s = FuzzyHashCheck(conf, 'test')
        s._get_hashes = get_hashes_mock
        viruses_out = s.scan_stream(filecontent)
        logger.debug("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertEqual({'fakeEXE.zip->fakeEXE.exe': 'zeroday.SSD/sc100'},viruses_out)

    @patch('fuglu.plugins.antivirus.get_session', side_effect=get_session_mock)
    def test_rarattachment(self,get_session_func):
        """Test if exe is found in rar - attachment and hashed correctly"""
        if not HAVE_SSDEEP:
            return

        myclass = self.__class__.__name__
        functionNameAsString = sys._getframe().f_code.co_name
        loggername = "%s.%s" % (myclass,functionNameAsString)
        logger = logging.getLogger(loggername)

        logger.debug("Config parser")
        confdefaults = r"""
[test]
dbconnectstring=
maxsize=1000000000000
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
threshold=75
virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf = FuConfigParser()
        conf.read_string(force_uString(confdefaults))

        logger.debug("Read file content")
        filecontent = BytesIO(mail_fakeEXEinrar).read()

        s = FuzzyHashCheck(conf, 'test')
        s._get_hashes = get_hashes_mock
        viruses_out = s.scan_stream(filecontent)
        logger.debug("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertEqual({'fakeEXE.rar->fakeEXE.exe': 'zeroday.SSD/sc100'},viruses_out)


class FuzzyhashTestScanStream(unittest.TestCase):

    def setUp(self):
        confdefaults = r"""
[test]
dbconnectstring=
maxsize=1000000000000
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
threshold=75
virusaction=DEFAULTVIRUSACTION
problemaction=DEFER
rejectmessage=threat detected: ${virusname}
filenamesrgx=\.(exe|scr|com|pif)$
messagetemplate=zeroday.SSD/sc${score}
"""
        conf = FuConfigParser()
        conf.read_string(force_uString(confdefaults))

        self.ssdeep = FuzzyHashCheck(conf, 'test')

    @patch('fuglu.plugins.antivirus.get_session', side_effect=get_session_mock)
    def test_non_brokenbase64(self, get_session_func):
        """Original case with correct file, from which the two broken cases below are derived"""
        filecontent = open(join(*[TESTDATADIR, "inline_image.eml"]), "rb").read()
        viruses_out = self.ssdeep.scan_stream(filecontent)
        print("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertIsNone(viruses_out)

    @patch('fuglu.plugins.antivirus.get_session', side_effect=get_session_mock)
    def test_brokenbase64(self, get_session_func):
        """Test version of "inline_image.eml" suddenly ending in the middle of base64 encoding, should not
        create exception"""

        filecontent = open(join(*[TESTDATADIR, "inline_image_broken.eml"]), "rb").read()
        viruses_out = self.ssdeep.scan_stream(filecontent)
        print("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertIsNone(viruses_out)

    @patch('fuglu.plugins.antivirus.get_session', side_effect=get_session_mock)
    def test_brokenbase64_2(self, get_session_func):
        """Test version of "inline_image.eml" with a missing character in the base64 encoding, should not
        create exception"""
        filecontent = open(join(*[TESTDATADIR, "inline_image_broken2.eml"]), "rb").read()
        viruses_out = self.ssdeep.scan_stream(filecontent)
        print("Output from scan_stream for viruses: %s" % str(viruses_out))
        self.assertIsNone(viruses_out)

Support & FAQ
=============

Contents:

.. toctree::
   :maxdepth: 2

---
FAQ
---

Does Fuglu run with Python3?
............................

Yes! Since version 0.8.0 (June 2018) Fuglu runs on Python3. Note that custom plugins might need to be adapted
to work correctly when switching from Python2 to Python3. Support for Python2 was dropped in version 0.10.7 (July 2020)
and minimum Python Version was increased to Python 3.6.

How do I upgrade/downgrade Fuglu?
.................................

Just install the new (or downgrade) version, overwriting the previous one. We usually try to avoid breaking changes.
Check the `changelog <https://gitlab.com/fumail/fuglu/blob/master/fuglu/doc/CHANGELOG>`_ to make sure.

How can I submit changes to the documentation?
..............................................

The fuglu documentation is the folder ``documentation/source``. Please edit the text files in this folder and submit a merge request.

How can I submit changes to fuglu itself?
.........................................

The source code is found in the folder ``fuglu/src/fuglu``. Please submit a merge request of your changes.
If unsure `open a ticket <https://gitlab.com/fumail/fuglu/-/issues>`_ first or ask in the `matrix chat room <https://matrix.to/#/#fuglu:matrix.org>`_ to discuss implementation questions.

Is there a community?
.....................

Due to restricted resources we currently limit interaction to two tools:

 * the `issue tracker <https://gitlab.com/fumail/fuglu/-/issues>`_ can be used for questions
 * we have a `matrix chat room <https://matrix.to/#/#fuglu:matrix.org>`_ for general discussions (`see also <https://gitlab.com/fumail/fuglu/-/issues/240>`_)

---------------
Asking for help
---------------

If you have a question about fuglu please open an issue using the "Question" template in the `issue tracker <https://gitlab.com/fumail/fuglu/-/issues>`_
